package shell;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
 
public class Shell {
	
	private int shellID;
	private TextArea shellText;
	private String textBuffer;
	
	public Shell(int id) {
		this.setShellID(id);
		this.shellText = new TextArea();
		this.shellText.setEditable(false);// Don't allow direct input. because we parse it trough a keylistener.
		this.shellText.setBackground(Color.BLACK);
		this.shellText.setForeground(Color.WHITE);
		this.shellText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				//System.out.println("Event caught");
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if((e.getKeyCode() > 60) &&  (e.getKeyCode() < 90)) {
					appendInput(""+e.getKeyChar());
				} else if(e.getKeyCode() == 10) {
					appendInput("\n<JSh>$");
				}
			}
		});
		this.setTextBuffer("<JSh>$");
		this.shellText.requestFocus();
	}
	
	public void appendInput(String inp) {
		this.textBuffer += inp;
		this.shellText.setText(textBuffer);
	}
	
	

	public String getTextBuffer() {
		return textBuffer;
	}



	public void setTextBuffer(String textBuffer) {
		this.textBuffer = textBuffer;
		this.shellText.setText(textBuffer);
	}



	public int getShellID() {
		return shellID;
	}


	public void setShellID(int shellID) {
		this.shellID = shellID;
	}


	public TextArea getShellText() {
		return shellText;
	}


	public void setShellTextSize(Dimension shellSize) {
		this.shellText.setSize(shellSize);
	}

}
