package windowhandler;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import shell.Shell;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	
	private String windowTitle;
	private int windowID;
	
	//Temporary vars
	/*
	 * window w = 400
	 * window h = 350
	 * 
	 * TODO: account size of characters inorder to size shell to characters and not pixels.
	 */
	
	public MainWindow(int id) {
		super();
		this.setSize(400, 350);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //TODO: Fix so it saves or otherwise takes care of closing opertaion.
		this.setLayout(new BorderLayout());
		
		this.windowTitle = "JTerm : ";
		this.setWindowID(id);
		
		this.setVisible(true);
	}
	
	public void addShell(Shell shell) {
		this.add(shell.getShellText(), BorderLayout.CENTER);
		this.windowTitle += ("JSh - " + shell.getShellID());
		this.setTitle(windowTitle);
	}

	public int getWindowID() {
		return windowID;
	}

	public void setWindowID(int windowID) {
		this.windowID = windowID;
	}

}
