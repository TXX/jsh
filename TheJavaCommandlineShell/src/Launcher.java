import shell.Shell;
import windowhandler.MainWindow;


public class Launcher {
	
	//DEBUG Switch
	public static boolean DBG = false;
	
	public static void main(String[] args) {
		if(args.length > 0) {
			DBG = true;
			System.out.println("Debug messages enabled.");
		}
		
		Shell mainShell = new Shell(1);
		MainWindow mw = new MainWindow(1);
		mw.addShell(mainShell);
		
	}

}
